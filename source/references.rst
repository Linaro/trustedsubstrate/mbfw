.. SPDX-License-Identifier: CC-BY-SA-4.0

##########
References
##########

.. [UEFI] `Unified Extensable Firmware Interface Specification v2.9
   <https://uefi.org/sites/default/files/resources/UEFI_Spec_2_9_2021_03_18.pdf>`_,
   February 2020, `UEFI Forum <http://www.uefi.org>`_

.. [FFA] `Arm Firmware Framework for Armv8-A
   <https://developer.arm.com/documentation/den0077/a/>`_,
   September 2018, `Arm Limited <http://arm.com>`_

.. [FWU] `Platform Security Firmware Update for the A-profile Arm Architecture 1.0
   <https://developer.arm.com/documentation/den0118/a>`_,
   May 2021, `Arm Limited <http://arm.com>`_

.. [EBBR] `Embedded Base Boot Requirements v2.0.0-pre1
   <https://arm-software.github.io/ebbr/>`_,
   January 2021, `Arm Limited <http://arm.com>`_

.. [PSBG] `Platform Security Boot Guide
   <https://developer.arm.com/documentation/den0072/0101>`_,
   July 2020, `Arm Limited <http://arm.com>`_

.. [NIST_800_193] `Platform Firmware Resiliency Guidelines
   <https://csrc.nist.gov/publications/detail/sp/800-193/final>`_,
   May 2018, NIST
