#################################################
Multi-banked firmware update (MBFW) specification
#################################################

**NOTE: This repo has moved under Arm. You can find the latest version** `here <https://github.com/ARM-software/edge-iot-arch-guide/tree/main/source/FWU/MBFW>`_.
==================================================================================================================================================================


License
=======

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
International License (CC-BY-SA-4.0). To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

Contributions are accepted under the same with sign-off under the Developer's
Certificate of Origin.

A copy of the license is included in the LICENSE_ file.

.. image:: https://i.creativecommons.org/l/by-sa/4.0/88x31.png
   :target: http://creativecommons.org/licenses/by-sa/4.0/
   :alt: Creative Commons License
.. _LICENSE: ./LICENSE
